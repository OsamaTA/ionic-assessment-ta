
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { DEFAULT_FEEDS } from './../constants/feeds.constants';
export class FeedItem {
  description: string;
  link: string;
  title: string;
  thumbnail:string;
  constructor(description: string, link: string, title: string,thumbnail:string) {
    this.description = description;
    this.link = link;
    this.title = title;
    this.thumbnail = thumbnail;
  }
}

export class Feed {
  title: string;
  url: string;
  status: Boolean;
  isReadOnly: Boolean;
  constructor(title: string, url: string, isReadOnly: boolean) {
    this.title = title;
    this.url = url;
    this.isReadOnly = isReadOnly;
  }
}


@Injectable()
export class RSSService {
  public url = 'https://query.yahooapis.com/v1/public/yql?q=select%20title%2Clink%2Cdescription%20from%20rss%20where%20url%3D%22http%3A%2F%2Ffeeds.feedburner.com%2Fraymondcamdensblog%3Fformat%3Dxml%22&format=json&diagnostics=true&callback=';
  constructor(private http: Http, public storage: Storage) { }
  public getSavedFeeds() {
    return this.storage.get('savedFeeds').then(data => {
      if (data !== null && data !== undefined) {
        return JSON.parse(data);
      } else {

        for (let df of DEFAULT_FEEDS) {
          let newFeed = new Feed(df.name, df.url, true);
          this.addFeed(newFeed).then(res => {
          });
        }
        return [];

      }
    });
  }



  public addFeed(newFeed: Feed) {
    return this.getSavedFeeds().then(arrayOfFeeds => {
      newFeed.status = true;
      if (newFeed.isReadOnly === undefined) {
        newFeed.isReadOnly = false
      }
      //alert(JSON.stringify(newFeed))
      arrayOfFeeds.push(newFeed)
      let jsonString = JSON.stringify(arrayOfFeeds);
      return this.storage.set('savedFeeds', jsonString);
    });
  }

  public getArticlesForUrl(feedUrl: string) {
   // var url = 'https://query.yahooapis.com/v1/public/yql?q=select%20title%2Clink%2Cdescription%20from%20rss%20where%20url%3D%22' + encodeURIComponent(feedUrl) + '%22%20limit%205&format=json';
    var url = 'https://api.rss2json.com/v1/api.json?rss_url=' + encodeURIComponent(feedUrl);
    //let articles = [];
    return this.http.get(url)
      .map(data => data.json())
      .map((res) => {
        console.log(res)
        if (res == null) {
          return [];
        }
        // let objects = res['item'];
        // var length = 20;

        // for (let i = 0; i < objects.length; i++) {
        //   let item = objects[i];
        //   var trimmedDescription = item.description.length > length ?
        //     item.description.substring(0, 80) + "..." :
        //     item.description;
        //   let newFeedItem = new FeedItem(trimmedDescription, item.link, item.title);
        //   articles.push(newFeedItem);
        // }

        return res['items'];
      })
  }


  updateStaticFeed(data) {
    let returnStatus: boolean = false;
    this.storage.get('savedFeeds').then((arrayOfFeeds) => {
      if (arrayOfFeeds) {
        let feedsData = JSON.parse(arrayOfFeeds);
        for (let i = 0; i < feedsData.length; i++) {
          if (feedsData[i].title == data.title) {
            if (data.status == "true" || data.status === true) {
              feedsData[i].status = true;
            } else {
              feedsData[i].status = false;
            }
            let newData = JSON.stringify(feedsData);
            this.storage.set('savedFeeds', newData);
            returnStatus = true;
            return returnStatus;
          }
        }
      }
    });
    return returnStatus;
  }

  removeFeed(data) {

    let returnStatus: boolean = false;
    this.storage.get('savedFeeds').then((arrayOfFeeds) => {
      if (arrayOfFeeds) {
        let feedsData = JSON.parse(arrayOfFeeds);
        for (let i = 0; i < feedsData.length; i++) {
          if (feedsData[i].title == data.title) {
            feedsData.splice(i, 1);
            let newData = JSON.stringify(feedsData);
            this.storage.set('savedFeeds', newData);
            returnStatus = true;
            
            return returnStatus;
          }
        }
      }
    });
  }

}