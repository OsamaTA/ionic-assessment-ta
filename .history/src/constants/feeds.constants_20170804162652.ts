import { FeedA } from './../models/feed';
export const DEFAULT_FEEDS: FeedA[] = [
	{ name: "Technology News", url: "http://feeds.bbci.co.uk/news/technology/rss.xml" },
	{ name: "World News", url: "http://feeds.bbci.co.uk/news/world/rss.xml" }
];