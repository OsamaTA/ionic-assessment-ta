import { Component, ViewChild } from '@angular/core';
import { Nav,Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { HeaderColor } from '@ionic-native/header-color';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(platform: Platform,statusBar: StatusBar,public storage:Storage, splashScreen: SplashScreen, public menuCtrl: MenuController,private headerColor: HeaderColor) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });
      this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'View Feeds', component: ListPage}
    ];
    this.headerColor.tint('#772dbc');
  }
  openMenu(p) {
    if(p==HomePage){

    }else{
    this.nav.push(p);
    }
  }

  closeMenu() {
    this.menuCtrl.close();
  }

  toggleMenu() {
    this.menuCtrl.toggle();
  }
}

