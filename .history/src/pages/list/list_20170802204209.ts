import { Component } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { RSSService, FeedItem, Feed } from '../../services/rss.service';



@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  articles: FeedItem[];
  selectedFeed: Feed;
  loading: Boolean;;
  constructor(private iab: InAppBrowser, private feedProvider: RSSService) {

  }

  public openArticle(url: string) {
    this.iab.create(url, '_blank');
    // window.open(url, '_blank');
  }


  loadArticles() {
    this.loading = true;
    this.feedProvider.getArticlesForUrl(this.selectedFeed.url).subscribe(res => {
      if (res.length > 0) {
        var newarr = new Array;
        newarr.push(...this.articles);
        for (let result of res) {
          if (result.description === undefined) {
            result.description = "";
          }
          newarr.push({ "title": result.title, "link": result.link, description: result.description })
          this.articles = newarr;

        }
      }
      this.loading = false;
    });
  }

  public extarctImage(str: string) {
  //  var str = "<img alt='' src='http://api.com/images/UID' /><br/>Some plain text<br/><a href='http://www.google.com'>http://www.google.com</a>";

    var regex = /<img.*?src='(.*?)'/;
    //alert(regex.exec(str)[1])
    return regex.exec(str)[1] || "";
  }

  public ionViewWillEnter() {

    if (this.selectedFeed !== undefined && this.selectedFeed !== null) {
      this.loadArticles()
    } else {
      this.feedProvider.getSavedFeeds().then(
        feeds => {
          if (feeds.length > 0) {
            for (let feed in feeds) {
              let item = feeds[feed];
              if (item.status) {
                this.selectedFeed = new Feed(item.title, item.url, false);

                this.loadArticles();
              } else {

              }
            }
          }
        }
      );
    }
  }
}