import { FeedService } from './../../services/feed.service';
import { RSSService, Feed } from './../../services/rss.service';
import { FeedA } from './../../models/feed';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { ListPage } from '../../pages/list/list';
import { ModalController } from 'ionic-angular';
import { AlertController, ToastController } from 'ionic-angular';
// import {  Nav, IonicPage } from 'ionic-angular';
// @IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public feedsa: FeedA[];
  public feeds: Feed[];
  constructor(public alertCtrl: AlertController, public modalCtrl: ModalController, public navCtrl: NavController, public feedService: FeedService, public menuCtrl: MenuController, public feedProvider: RSSService, private toastCtrl: ToastController) { }

  public ionViewDidLoad() {
    this.feedsa = this.feedService.getFeeds();

  }

  openMenu() {
    this.menuCtrl.open();
  }

  closeMenu() {
    this.menuCtrl.close();
  }

  toggleMenu() {
    this.menuCtrl.toggle();
  }
  openPage(entry) {
    console.log('open page called with ' + entry.title);
    this.navCtrl.push(ListPage, { selectedEntry: entry });
  }


  pushPage() {
    this.navCtrl.push(ListPage);
  }

  public addFeed() {
    let prompt = this.alertCtrl.create({
      title: 'Add Feed URL',
      inputs: [
        {
          name: 'name',
          placeholder: 'The best Feed ever'
        },
        {
          name: 'url',
          placeholder: 'http://www.myfeedurl.com/feed'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Save',
          handler: data => {
            if (data.name && data.url) {
              let newFeed = new Feed(data.name, data.url, false);
              this.feedProvider.addFeed(newFeed).then(
                res => {
                  this.loadFeeds();
                  this.presentToast("Feed added successfully");
                }
              );
            } else {
              alert("Plese fill the form");
              return false;
            }
          }
        }
      ]
    });
    prompt.present();
  }

  presentToast(msg: string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });

    toast.present();
  }
  loadFeeds() {
    this.feedProvider.getSavedFeeds().then(
      allFeeds => {
        this.feeds = allFeeds;

        //  for(let StaticFeed of this.feedsa){
        //    alert(StaticFeed.name)
        //  }
      });
  }

  public openFeed(feed: Feed) {
    this.navCtrl.setRoot(ListPage, { 'selectedFeed': feed });
  }

  public ionViewWillEnter() {
    this.loadFeeds();
  }

  public updateStaticFeeds(feed) {
    return this.feedProvider.updateStaticFeed(feed);
  }
  public removeFeeds(feed) {
    this.feedProvider.removeFeed(feed);
    this.presentToast("Feed deleted successfully");
    this.navCtrl.setRoot(HomePage);
    return true;
  }


}
