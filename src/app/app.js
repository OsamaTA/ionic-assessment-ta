import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  'fontFace': {
    'fontFamily': 'Raleway_md',
    'src': 'url("../assets/fonts/Raleway-md.woff2") format("woff2")'
  },
  'fontFace': {
    'fontFamily': 'Raleway_sb',
    'src': 'url("../assets/fonts/Raleway-sb.woff2") format("woff2")'
  },
  'fontFace': {
    'fontFamily': 'Lato',
    'src': 'url("../assets/fonts/Lato.woff2") format("woff2")'
  },
  '*': {
    'fontFamily': 'Raleway_md'
  },
  'ion-title': {
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'width': [{ 'unit': '%H', 'value': 1 }],
    'height': [{ 'unit': '%V', 'value': 1 }],
    'textAlign': 'center',
    'zIndex': '-1'
  },
  'p-side_menu': {
    'height': [{ 'unit': 'px', 'value': 195 }]
  },
  'p-side_menu toolbar-background': {
    'backgroundImage': 'url(../assets/images/side_menu.png)',
    'backgroundSize': 'cover',
    'backgroundPosition': 'center'
  },
  'p-side_listitem': {
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'p-side_list item-inner': {
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 0 }]
  },
  'p-item_wrap': {
    'display': 'flex',
    'alignItems': 'center'
  },
  'p-item_wrap img': {
    'marginRight': [{ 'unit': 'px', 'value': 20 }],
    'marginLeft': [{ 'unit': 'px', 'value': 30 }],
    'width': [{ 'unit': 'px', 'value': 20 }],
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  }
});
