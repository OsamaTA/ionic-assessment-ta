import { DEFAULT_FEEDS } from './../constants/feeds.constants';
import { FeedA } from './../models/feed';
import { Injectable } from "@angular/core";

@Injectable()
export class FeedService {

	public getFeeds(): FeedA[] {
		return DEFAULT_FEEDS;
	}

}