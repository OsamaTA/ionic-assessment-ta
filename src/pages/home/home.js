import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  'page-home cust_lab': {
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'display': 'block',
    'overflow': 'hidden',
    'WebkitBoxFlex': '1',
    'WebkitFlex': '1',
    'MsFlex': '1',
    'flex': '1',
    'fontSize': [{ 'unit': 'string', 'value': 'inherit' }],
    'textOverflow': 'ellipsis',
    'whiteSpace': 'nowrap'
  },
  'page-home bx-wraper': {
    'display': '-webkit-box',
    'display': '-webkit-flex',
    'display': '-ms-flexbox',
    'display': 'flex',
    'overflow': 'hidden',
    'WebkitBoxFlex': '1',
    'WebkitFlex': '1',
    'MsFlex': '1',
    'flex': '1',
    'WebkitBoxOrient': 'vertical',
    'WebkitBoxDirection': 'normal',
    'WebkitFlexDirection': 'inherit',
    'MsFlexDirection': 'inherit',
    'flexDirection': 'inherit',
    'WebkitBoxAlign': 'inherit',
    'WebkitAlignItems': 'inherit',
    'MsFlexAlign': 'inherit',
    'alignItems': 'inherit',
    'WebkitAlignSelf': 'stretch',
    'MsFlexItemAlign': 'stretch',
    'alignSelf': 'stretch',
    'textOverflow': 'ellipsis'
  },
  'page-home middel_btn': {
    'alignItems': 'center',
    'display': '-webkit-box',
    'display': '-webkit-flex',
    'display': '-ms-flexbox',
    'display': 'flex'
  },
  'page-home p-head_txt': {
    'color': '#fff',
    'textTransform': 'uppercase'
  },
  'page-home p-head_wrap': {
    'display': 'flex',
    'alignItems': 'center'
  },
  'page-home p-head_wrap p-menu_toggle': {
    'order': '0',
    'color': '#fff'
  },
  'page-home p-head_wrap p-add': {
    'color': '#fff',
    'marginLeft': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'page-home scroll-content': {
    'display': 'flex',
    'flexDirection': 'column'
  },
  'page-home p-feed_btn': {
    'marginTop': [{ 'unit': 'string', 'value': 'auto' }],
    'marginBottom': [{ 'unit': 'px', 'value': 20 }]
  },
  'page-home p-feed_btn button': {
    'background': '#0093cf',
    'fontSize': [{ 'unit': 'rem', 'value': 1.8 }]
  },
  'page-home p-feed_list ion-item': {
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'page-home p-feed_list item-inner': {
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ebebeb' }, { 'unit': 'string', 'value': '!important' }]
  },
  'page-home p-feed_list p-feed_head': {
    'color': '#535353',
    'fontSize': [{ 'unit': 'rem', 'value': 1.8 }],
    'marginBottom': [{ 'unit': 'px', 'value': 5 }]
  },
  'page-home p-feed_list p-feed_link': {
    'color': '#707070'
  },
  'alert-wrapper': {
    'height': [{ 'unit': 'px', 'value': 300 }],
    'minWidth': [{ 'unit': 'px', 'value': 300 }]
  },
  'alert-wrapper alert-head': {
    'background': '#0093cf',
    'color': '#fff'
  },
  'alert-wrapper alert-button-group': {
    'marginTop': [{ 'unit': 'string', 'value': 'auto' }]
  }
});
