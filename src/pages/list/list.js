import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  'page-list #feed-spinner': {
    'margin': [{ 'unit': 'string', 'value': 'auto' }, { 'unit': 'string', 'value': 'auto' }, { 'unit': 'string', 'value': 'auto' }, { 'unit': 'string', 'value': 'auto' }],
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'height': [{ 'unit': 'px', 'value': 100 }]
  },
  'page-list p-head_txt': {
    'color': '#fff',
    'textTransform': 'uppercase'
  },
  'page-list back-button': {
    'color': '#fff'
  },
  'page-list card-md p-heading': {
    'color': '#333',
    'fontWeight': 'bold'
  },
  'page-list card-md pp-desc': {
    'fontSize': [{ 'unit': 'rem', 'value': 1.2 }],
    'color': '#666'
  },
  'page-list card-md p-pub_date': {
    'fontFamily': 'Lato'
  }
});
